module.exports = {
    testEnvironment: 'node', // For Node.js projects
    // testEnvironment: 'jsdom', // For React projects
  
    // Optional: Specify the directories where your tests are located
    testMatch: ['**/__tests__/**/*.js', '**/?(*.)+(spec|test).js'],
  
    // Optional: Specify any other Jest configuration options you need
    // For example, code coverage settings, test reporters, etc.
  };